//
//  personalViewController.h
//  householdApp
//
//  Created by XPG on 14-5-5.
//  Copyright (c) 2014年 XPG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface personalViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) IBOutlet UITableView *tableview;
@property(nonatomic,strong) NSMutableArray* UIDataArr;//ui数据数组
@end
