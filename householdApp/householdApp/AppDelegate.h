//
//  AppDelegate.h
//  householdApp
//
//  Created by XPG on 14-5-5.
//  Copyright (c) 2014年 XPG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeveyTabBarController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) LeveyTabBarController *leveyTabBarController;
@end
